package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	_ "io"
	"io/ioutil"
	"log"
	"net/http"
	_ "os"
	_ "path"
	_ "path/filepath"
	"strconv"
	"strings"

	yaml "gopkg.in/yaml.v2"
)

type webhookReqBody struct {
	Message struct {
		Text string `json:"text"`
		User struct {
			ID       int64  `json:"id"`
			IsBot    bool   `json:"is_bot"`
			Username string `json:"username"`
		} `json:"from"`
		Chat struct {
			ID int64 `json:"id"`
		} `json:"chat"`
	} `json:"message"`
}

// This handler is called everytime telegram sends us a webhook event
func Handler(res http.ResponseWriter, req *http.Request) {
	// First, decode the JSON response body
	body := &webhookReqBody{}
	if err := json.NewDecoder(req.Body).Decode(body); err != nil {
		fmt.Println("could not decode request body", err)
	}

	// Check if the message contains the command
	// if not, return without doing anything
	command := strings.ToLower(body.Message.Text)
	message := body.Message.Text

	switch {
	case command == "/дай текст":
		if err := sendText(strconv.Itoa(int(body.Message.Chat.ID)), "случайный текст"); err != nil {
			fmt.Println("error in sending reply: ", err)
		}
	case command == "/дай текст в канале":
		if err := sendText(config.ChannelUsername, "случайный текст"); err != nil {
			fmt.Println("error in sending text in channel: ", err)
		}
	case command == "/дай мемов":
		if err := sendPhotoByLink(config.ChannelUsername, config.MockLink, "случайная подпись", false); err != nil {
			fmt.Print("error in sending photo by link in channel: ", err)
		}
	case strings.Contains(command, "/запостить текст"):
		if err := sendText(config.ChannelUsername, strings.ReplaceAll(message, "/запостить текст", "")); err != nil {
			fmt.Println("error in posting text: \""+message+"\" in channel: ", err)
		}
	case strings.Contains(command, "/запостить мем"):
		link := strings.ReplaceAll(message, "/запостить мем", "")
		mess := ""
		indx := strings.Index(link, "http")
		if indx != 0 {
			link = link[indx:]
		}

		indx = strings.Index(link, " ")
		if indx != -1 {
			mess = link[indx+1:]
			link = link[:indx]
		}

		if err := sendPhotoByLink(config.ChannelUsername, link, mess, false); err != nil {
			fmt.Println("error in posting text: \""+message+"\" in channel: ", err)
		}
	}

}

type photoByLink struct {
	ChatID     string `json:"chat_id"`
	Caption    string `json:"caption"`
	SilentMode string `json:"disable_notification"`
	Photo      string `json:"photo"`
}

func sendPhotoByLink(ChannelUsername string, linkPhoto string, caption string, silentMode bool) error {
	url := "https://api.telegram.org/bot" + config.TokenBot + "/sendPhoto"

	photoBody := &photoByLink{
		ChatID:     ChannelUsername,
		Caption:    caption,
		SilentMode: strconv.FormatBool(silentMode),
		Photo:      linkPhoto,
	}

	jsonPB, err := json.Marshal(photoBody)
	if err != nil {
		return err
	}
	payload := bytes.NewBuffer(jsonPB)

	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		return err
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("User-Agent", "nokex")
	req.Header.Add("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()
	// body, err := ioutil.ReadAll(res.Body)
	// if err != nil {
	// 	return err
	// }

	// // fmt.Println(res)
	// fmt.Println(string(body))
	return nil
}

type sendTextChannelBody struct {
	ChannelUsername string `json:"chat_id"`
	Text            string `json:"text"`
}

func sendText(channelUsername string, text string) error {
	reqBody := &sendTextChannelBody{
		ChannelUsername: channelUsername,
		Text:            text,
	}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	// Send a post request with your token
	res, err := http.Post("https://api.telegram.org/bot"+config.TokenBot+"/sendMessage", "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}

	fmt.Println("textpost has been sent")
	return nil
}

type Config struct {
	ChannelUsername string
	TokenBot        string
	MockLink        string
}

func configure() (Config, error) {
	yfile, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		return Config{}, err
	}

	config := Config{}

	err = yaml.Unmarshal([]byte(yfile), &config)
	if err != nil {
		return Config{}, err
	}

	config.ChannelUsername = "@" + config.ChannelUsername

	return config, nil
}

var config Config

func main() {
	var err error
	config, err = configure()
	if err != nil {
		log.Fatal(err)
	}

	if err := http.ListenAndServe(":3000", http.HandlerFunc(Handler)); err != nil {
		fmt.Print("error: ", err)
	}
}
