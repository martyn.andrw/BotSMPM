ngrok:
	curl -F "url=https://7473-176-118-67-3.ngrok.io/"  "https://api.telegram.org/bot<token>/setWebhook"

init:
	ls go.mod || go mod init gitlab.com/martyn.andrw/BotSMPM
	go mod tidy

run:
	go run main.go