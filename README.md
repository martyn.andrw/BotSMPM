<h1>Telegram bot for posting memes</h1>
Bot can post text message, photo in the channel at your request in a message to the bot.                    <br>
Command example:                                                                                            <br>
    - /дай текст - posts some text message in your chat                                               <br>
    - /дай текст в канале - posts some text message in your channel                                         <br>
    - /дай мемов - posts photo with caption in your channel                                                 <br>
    - /запостить текст '<i>your text</i>' - posts <i>your text</i> in channel                               <br>
    - /запостить мем '<i>link to your photo' 'caption'</i> - posts your photo with a caption in channel     <br>
The bot is still in development, some commands are likely to change                                         <br>

<h2>Телеграм бот для постинга мемов</h2>
Бот постит текстовые сообщения, фото в группе по запросу при общении с ботом                                <br>
Пример запросов/команд:                                                                                     <br>
    - /дай текст - постит некоторое сообщение в вашем чатике с ботом                                  <br>
    - /дай текст в канале - постит некоторое сообщение в группе                                             <br>
    - /дай мемов - постит некоторый мем с некоторым сообщением в группе                                     <br>
    - /запостить текст '<i>ваш текст</i>' - постит <i>ваш текст</i> в группе                                <br>
    - /запостить мем '<i>ссылка на фотографию' 'текст'</i> - постит вашу фотографию с подписью в группе     <br>
Бот все еще в разработке, некоторые команды скорее всего изменятся                                          <br>

<h3>TODO</h3>
- Сделать отправку файлов
- Сделать интеграцию со своими микросервисами 